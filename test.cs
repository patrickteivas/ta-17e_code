using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediaGallery.Commands;
using MediaGallery.Data;
using MediaGallery.FileSystem;
using MediaGallery.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Formats;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;

namespace MediaGallery.Services
{
    public class PhotoService : IPhotoService
    {
        private readonly ApplicationDbContext _dataContext;
        private readonly IMapper _objectMapper;
        private readonly SavePhotoCommand _savePhotoCommand;
        private readonly GalleryContext _galleryContext;
        private readonly IFileClient _fileClient;

        public PhotoService(ApplicationDbContext dataContext, IMapper objectMapper, SavePhotoCommand savePhotoCommand, GalleryContext galleryContext, IFileClient fileClient)
        {
            _dataContext = dataContext;
            _objectMapper = objectMapper;
            _savePhotoCommand = savePhotoCommand;
            _galleryContext = galleryContext;
            _fileClient = fileClient;
        }

        public async Task<FrontPageModel> GetFrontPageData()
        {
            var model = new FrontPageModel();

            model.NewPhotos = await _dataContext.Photos
                                    //.Cast<MediaItem>()
                                    //.Include(p => p.ParentFolder)
                                    .ProjectTo<PhotoListModel>()
                                    .ToListAsync();

            model.PopularPhotos = model.NewPhotos;
            return model;
        }

        public async Task<List<string>> UploadFile(IList<IFormFile> files, int? parentFolder)
        {
            var list = new List<string>();

            foreach (var file in files)
            {
                var model = new PhotoEditModel();
                model.FileName = Path.GetFileName(file.FileName);
                model.Thumbnail = Path.GetFileName(file.FileName);
                model.ParentFolderId = parentFolder;
                model.File = file;

                list.AddRange(await _savePhotoCommand.Validate(model));

                await _savePhotoCommand.Execute(model);
            }

            return list;
        }

        public async Task<GetFileWithEffectModel> GetFileWithEffect(int id, string effect)
        {
            var model = new GetFileWithEffectModel();

            // async returns object disposed exception
            var item = _dataContext.Items
                       .Include(p => p.ParentFolder)
                       .FirstOrDefault(i => i.Id == id);

            var fileItem = (Photo)item;
            var folder = fileItem.ParentFolder;
            var path = fileItem.FileName;
            if (folder != null)
            {
                path = await _galleryContext.GetFolderPath(folder.Id, fileItem.FileName);
            }
            model.FileName = fileItem.FileName;

            IImageFormat format;
            using (var fileStream = _fileClient.GetFile(path))
            using (var image = Image.Load(fileStream, out format))
            {
                Image<Rgba32> imageWithEffect;

                if (effect == "BlackWhite")
                {
                    imageWithEffect = image.Clone(ctx => ctx.BlackWhite());
                }
                else if (effect == "OilPaint")
                {
                    imageWithEffect = image.Clone(ctx => ctx.OilPaint());
                }
                else if (effect == "Sepia")
                {
                    imageWithEffect = image.Clone(ctx => ctx.Sepia());
                }
                else if (effect == "Blur")
                {
                    imageWithEffect = image.Clone(ctx => ctx.GaussianBlur());
                }
                else if (effect == "Sharpen")
                {
                    imageWithEffect = image.Clone(ctx => ctx.GaussianSharpen());
                }
                else if (effect == "Glow")
                {
                    imageWithEffect = image.Clone(ctx => ctx.Glow());
                }
                else if (effect == "Invert")
                {
                    imageWithEffect = image.Clone(ctx => ctx.Invert());
                }
                else
                {
                    imageWithEffect = image;
                }

                model.ImageWithEffect = imageWithEffect;
                model.Format = format;
            }

            return model;
        }
    }
}